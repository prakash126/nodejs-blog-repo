const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
  const { token } = req.cookies;
  jwt.verify(token, "$#S$#&12345@**", (err, decoded) => {
    if (err) {
      res.render('pages/login', {
        path: '/auth/login',
        msg: {
          isMsgAvailable: true,
          header: 'Authentication Falied!',
          body: 'Login Again',
          type: 'error',
        },
        title: 'Login',
      });
    } else {
      req.user = {
        id: decoded.userId,
        uname: decoded.uname,
        email: decoded.email,
      };
      next();
    }
  });
};
