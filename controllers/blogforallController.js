const Blog = require('../models/blog');

exports.getBlogList = (req,res,next)=>{
  Blog.find().then((blogdetails)=>{
    res.render('pages/blog-list',{
      path:'/',
      title: 'Home',
      blog: blogdetails,
    });
  }).catch((err)=>{
    console.log(err);
  })
}

exports.searchBlogList = (req,res,next)=>{
  
  // /console.log(req.body.search)
  Blog.find({title:{$regex: req.body.search}}).sort({date:-1}).then((result)=>{
    // console.log(result)
    res.render('pages/blog-list',{
      path:'/',
      title: 'Home',
      blog: result,
      search: req.body.search,
    })
  }).catch((err)=>{
    console.log(err);
  })
}

exports.getOneBlog = (req, res) => {
  Blog.findOne({ slug: req.params.slug })
    .then((blog) => {
      if (!blog) {
        res.redirect('/');
      } else {
        res.render('pages/blog', {
          path: '/one-blog',
          title: blog.title,
          blog:blog,
        });
      }
    })
    .catch((err) => {
      console.log(err);
    });
};