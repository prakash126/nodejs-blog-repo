const Category = require('../models/category');
const Blog = require('../models/blog');
var mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;

exports.getAllBlog = (req, res) => {
  //res.send(req.body)
  Blog.find({ userId: req.user.id })
    .sort({ date: -1 })
    .then((allBlogs) => {
      res.render('pages/dashboard/all-blog', {
        path: '/dashboard/all-blog',
        title: 'Dashboard | All Blogs',
        blogs: allBlogs,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

var categorydetails = []
exports.getAllCategory = (req,res,next)=>{
  Category.find({userId:req.user.id}).then((categories)=>{
    for(cat of categories){
      categorydetails.push(cat)
    }
    res.render('pages/dashboard/all-categories',{
      path:'/dashboard/all-categories',
      title: 'Dashboard | All Category',
      category: categories,
    });
  })
}

exports.getAddCateogory = (req, res,next) => {
  res.render('pages/dashboard/add-category', {
    path: '/dashboard/add-category',
    title: 'Dashboard | Add Category',
  });
};

exports.postAddCategory = (req, res) => {
  if (req.body.new_category === '') {
    res.render('pages/dashboard/add-category', {
      path: '/dashboard/add-category',
      
      title: 'Dashboard | Add Category',
    });
  } else {
    Category.findOne({name:req.body.new_category}).then((result)=>{
      if(result){
        console.log('Category existed');
         return res.redirect('/dashboard/all-categories');
      }else{
        const newCategory = new Category({
          name: req.body.new_category,
          userId:ObjectId(req.user.id),
        });
        newCategory
          .save()
          .then(() => {
            res.redirect('/dashboard/all-categories');
          })
          .catch((err) => {
            res.render('pages/dashboard/add-category', {
              path: '/dashboard/add-category',
              title: 'Dashboard | Add Category',
            });
          });
      }
    }).catch((err)=>{
      console.log(err);
    })
    
  }
};

exports.deleteCategory = (req, res,next) => {
  //var id = req.body.categoryid.toString();
 // console.log(req.body.categoryid)
  Category.findByIdAndRemove(req.body.categoryid).then((deleted)=>{
    if(!deleted){
      res.send('deleted');
    }
    else{
      console.log('deleted');
      res.redirect('/dashboard/all-categories')
    }
  })

};



exports.getEditCategory = (req,res,next)=>{
    //console.log(req.body);
  Category.findById({_id:req.body.categoryid}).then((category)=>{
  //console.log(category.name)
    
    res.render('pages/dashboard/edit-category', {
    path: '/dashboard/edit-category',
    category:category,
    title: 'Dashboard | Edit Category',
  });
  }).catch((err)=>{
    console.log(err)
  })
};

exports.postEditCategory = (req,res,next)=>{
Category.findOne({name:req.body.catname}).then((result)=>{
  if(result){
    console.log('already exits');
    //console.log(result)
    res.redirect('/dashboard/all-categories');
  }
  else{
    Category.update({_id:req.body.categoryid},{$set: { name: req.body.catname }}).then((result)=>{
      console.log(result)
      console.log('updated')
      res.redirect('/dashboard/all-categories')
    }).catch((err)=>{
      console.log(err)
    })
  }
}).catch((err)=>{
  console.log(err);
})
  
}

/*Blog*/



exports.getAddBlog = (req, res,next) => {
  Category.find({ userId: req.user.id })
    .then((categories) => {
      res.render('pages/dashboard/add-blog', {
        path: '/dashboard/add-blog',
        title: 'Dashboard | Add Blog',
        category: categories,
        blog: {
          title: '',
          slug: '',
          category: '',
          content: '',
        },
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.postAddBlog = (req,res,next)=>{
       const newBlog = new Blog({
          title: req.body.title,
          slug: Math.floor(Math.random() * (98532 - 12365) + 12689),
          category: req.body.category,
          imageUrl: '/uploads/' + req.file.filename,
          publishDate: new Date().toISOString(),
          userId: req.user.id,
          description: req.body.desc,
          author: req.user.uname,
      });
      newBlog.save().then(()=>{
        res.redirect('/dashboard')
      }).catch((err)=>{
        console.log(err);
      })
}


exports.deleteBlog = (req, res,next) => {
  Blog.findByIdAndRemove(req.body.blogid).then((isdeleted)=>{
    if(!isdeleted){
      console.log(isDeleted)
    }
    else{
      console.log('deleted');
      res.redirect('/dashboard/all-blog')
    }
  })
};

exports.getEditkroBlog=(req,res,next)=>{
  Blog.findById(req.query.bgid).then((blogDetails)=>{
    //console.log(blogDetails)
    // res.send(blogDetails);
    //res.render('pages/dashboard/editblog')
    Category.find({userId:req.user.id}).then((catdetails)=>{
      res.render('pages/dashboard/editblog',{
        path:'/dashboard/edit-blog',
        title: 'Edit Blog',
        category:catdetails,
        blog:blogDetails,
      })
    }).catch((err)=>{
      console.log(err)
    })
  }).catch((err)=>{
    console.log(err)
  })
}

exports.postEditkroBlog =(req,res,next)=>{
  //res.send(req.body)
  console.log(req.body)
  Blog.updateOne({_id:req.body.bgId},{$set:{
    title:req.body.title,slug:req.body.slug,imageUrl: '/uploads/' + req.file.filename,category:req.body.category,description:req.body.description
  }}).then((result)=>{
    // res.send(result)
    res.redirect('/dashboard')
  })
}

/*blog end */


