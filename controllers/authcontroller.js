
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('../models/loginuser');

exports.getloginpage = (req,res,next)=>{
  res.render('pages/login',{
    path:'/auth/login',
    title:'Login',
  })
}

exports.postloginpage = (req,res,next)=>{
 // res.send(req.body)
 var obj = {
   email:req.body.email,
   password:req.body.password
 }
 //res.send(obj)
 User.findOne({email:obj.email}).then((user)=>{
    if(user){
    bcrypt.compare(obj.password,user.password,(err,matched)=>{
     // res.send('MAtched')
     if(matched){
       //console.log('matched')
      const token = jwt.sign(
        {
          userId: user._id.toString(),
          email: user.email,
          uname: user.uname,
        },
        "$#S$#&12345@**",
        {
          expiresIn:'1h'
        }
      );
        res.cookie('token', token, {
          maxAge: 3600000,
          httpOnly: true,
          sameSite: 'strict',
      });
      
      return res.redirect('/dashboard');
     }
     else{
      return res.render('pages/login', {
                  path: '/auth/login',
                  title: 'Login',
                });
     }
    })
  }
  else{
    return res.render('pages/login', {
              path: '/auth/login',
              title: 'Login',
            });
  }
 })
}
exports.Logout = (req, res) => {
  res.cookie('token', '', {
    maxAge: 0,
    httpOnly: true,
    sameSite: 'strict',
  });
  if(200){
    res.redirect('/');
  }
};
