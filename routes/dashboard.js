const rt = require('express').Router();
const dashController = require('../controllers/dashcontroller');
var multer  = require('multer')

const storageSetup = multer.diskStorage({
    destination: 'data/uploads',
    filename: (req, file, cb) => {
      cb(null, Date.now() + file.originalname);
    },
  });
  const fileFilterSetup = (req, file, cb) => {
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
      cb(null, true);
    } else {
      cb(null, false);
    }
  };

  const upload = multer({ storage: storageSetup, fileFilter: fileFilterSetup });


rt.get('/',dashController.getAllBlog);

rt.get('/all-blog', dashController.getAllBlog);




rt.get('/all-categories', dashController.getAllCategory);

rt.get('/add-category', dashController.getAddCateogory);

rt.post('/add-category', dashController.postAddCategory);

rt.post('/delete-category', dashController.deleteCategory);


rt.post('/get-edit-category',dashController.getEditCategory)
rt.post('/post-edit-category', dashController.postEditCategory);

rt.get('/add-blog', dashController.getAddBlog);

rt.post('/add-blog', upload.single('image'), dashController.postAddBlog)

rt.post('/delete-blog', dashController.deleteBlog);

rt.get('/edit-blog',upload.single('image'), dashController.getEditkroBlog);

rt.post('/edit-blog',upload.single('image'),dashController.postEditkroBlog);

module.exports = rt;
