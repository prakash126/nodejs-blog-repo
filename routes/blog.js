const router = require('express').Router();
const blogforallController = require('../controllers/blogforallController');

router.get('/', blogforallController.getBlogList);
router.post('/search',blogforallController.searchBlogList);
router.get('/:slug', blogforallController.getOneBlog);

module.exports = router;
