const router = require('express').Router();
const authController = require('../controllers/authcontroller');

router.get('/login', authController.getloginpage);

router.post('/login', authController.postloginpage);

router.post('/logout', authController.Logout);

module.exports = router;
