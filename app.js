const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const path = require('path');

flash = require('express-flash');
session = require('express-session');
// routes
const blogRoutes = require('./routes/blog');
const authRoutes = require('./routes/authroutes');
const dashboardRoutes = require('./routes/dashboard');

// middleware
const isAuth = require('./middleware/is-auth');

// util function for dummy admin
const createAdmin = require('./utils/admin');

// Configuring enviroment variables
//require('dotenv').config();

// Creating express app
const app = express();


// middlewares
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'data')));



  
// setting up default rendering engine
app.set('view engine', 'ejs');

app.use(session({
    //cookie: { maxAge: 60000 },
    //store: sessionStore,
    secret: 'I am praksh',
    resave: false,
    saveUninitialized: true   
}));
app.use(flash());
// setting up Routes
app.use('/auth', authRoutes);
app.use('/dashboard',isAuth,dashboardRoutes);
app.use(blogRoutes);



// connecting to DB
mongoose
  .connect('mongodb://localhost:27017/blog', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log('Connected to DB!');
    createAdmin()
      .then(() => {
        bootstrap();
      })
      .catch((err) => {
        console.log(err);
      });
   
  })
  .catch((err) => {
    console.log(err);
  });

// bootstrap function to be called after successfull connection to database
const bootstrap = () => {
  // Listening to assigned port
  var port = 3000
  app.listen(port, () => {
    console.log('App listening to PORT: ' + port);
  });
};