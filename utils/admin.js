const bcrypt = require('bcrypt');
const User = require('../models/loginuser');
module.exports = () => {
  return new Promise((resolve, reject) => {
    User.find().then((users) => {
      if (users.length === 0) {
        bcrypt.hash('prakash', 12, (err, hash) => {
          if (err) {
            console.log(err);
            resolve();
          }
          const newUser = new User({
            email: 'prakashk@zignuts.com',
            uname: 'Prakash Kumar',
            password: hash,
          });

          newUser.save().then(() => {
            console.log(
              'Signup Successfuly! prakashk@zignuts.com Password: prakash'
            );
            resolve();
          });
        });
      } else {
        console.log(
          'Already Exits! Email: prakashk@zignuts.com Password: prakash'
        );
        resolve();
      }
    });
  });
};
