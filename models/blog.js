const mongoose = require('mongoose');


const blogSchema = mongoose.Schema({
  title:{
    type:String,
    required:true
  },
  slug:{
    type:String,
    required:true
  },
  category:{
    type:String,
    required:true
  },
  description:{
    type:String,
    required:true
  },
  publishDate:{
    type:String,
    required:true
  },
  imageUrl:{
    type:String,
    required:true
  },
  userId:{
    type:String,
    required:true
  },
  author:{
    type:String,
    required:true
  }

})

module.exports = mongoose.model('blog',blogSchema)