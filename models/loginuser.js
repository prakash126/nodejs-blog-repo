const mongoose = require('mongoose');

const userSchema  = mongoose.Schema({
    email:{
        type:String,
        required:true
    },
    uname:{
        type:String,
        required:true
    },
    password:{
        type:String,
        required:true
    }
})

module.exports = mongoose.model('user',userSchema);
// here the collection name is by default purals like users